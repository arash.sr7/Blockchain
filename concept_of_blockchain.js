const sha256=require("crypto-js/sha256")

class Block{
	constructor(index,timestamp,data,previousHash=''){
		this.index=index;
		this.timestamp=timestamp;
		this.data=data;
		this.previousHash=previousHash;
		this.hash=this.calculateHash();
	}
	calculateHash(){
		return sha256(this.index + this.timestamp + JSON.stringify(this.data) + this.previousHash).toString();
	}
}

class Blockchain{
	constructor(){
		this.chain=[this.createGenesisBlock()];
	}
	createGenesisBlock(){
		return new Block(0,"01/01/2017","Genesis block","0");
	}
	getLatestBlock(){
		return this.chain[this.chain.length-1];
	}
	addBlock(newBlock){
		newBlock.previousHash=this.getLatestBlock().hash;
		newBlock.hash=newBlock.calculateHash();
		this.chain.push(newBlock);
	}
	isChainValid(){
		for (var i=1;i<=this.chain.lenght;i++){
			if (this.chain[i].hash !== this.chain[i].calculateHash()) { return false;}
			if (this.chain[i].previousHash !== this.chain[i-1].hash) { return false;}
		}
		return true;

	}
}

let arash_coin = new Blockchain();
arash_coin.addBlock(new Block(1,"02/01/2017",{amount: 3}));
arash_coin.addBlock(new Block(1,"03/01/2017",{amount: 6}));

console.log(JSON.stringify(arash_coin ,null ,4));